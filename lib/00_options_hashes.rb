def transmogrify(string, options = {})
  defaults = {
    times: 1,
    upcase: false,
    reverse: false
  }
  options = defaults.merge(options)

  new_str = ""
  options[:times].times { new_str << string }
  new_str.upcase! if options[:upcase]
  new_str.reverse! if options[:reverse]
end
